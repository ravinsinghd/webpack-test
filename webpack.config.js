module.exports = {
  entry: ['./polyfill.ts', './app.ts', ],
  output: {
    filename: 'bundle.js'
  },
  module: {
    rules: [{
      test: /\.ts?$/,
      use: 'ts-loader',
      exclude: /node_modules/
    }]
  },
}